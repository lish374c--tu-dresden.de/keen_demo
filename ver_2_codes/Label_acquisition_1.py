import os
import cv2
import numpy as np
import json
from PIL import Image, ExifTags

# changing to working directory
os.path.dirname(os.getcwd())
main_path = os.getcwd()
os.chdir(main_path)
directory = "input_files_keen/FullWaterDatasetwithMetadata"

class ImageLabels:
    def __init__(self, input_sizeX=48, input_sizeY=36):
        self.input_sizeX = input_sizeX
        self.input_sizeY = input_sizeY
        self.exif_data = None
        self.data = []
        self.labels = []
        self.image_size = (self.input_sizeX, self.input_sizeY)
        self.directory = "input_files_keen/FullWaterDatasetwithMetadata"

    def get_exif(self, exif_data_content):
        decoded_exif = {}
        for tag, value in exif_data_content.items():
            tag_name = ExifTags.TAGS.get(tag, tag)
            decoded_value = value
            if isinstance(value, bytes):
                try:
                    decoded_value = value.decode("utf-8")
                except UnicodeDecodeError:
                    decoded_value = "Unable to decode"
            decoded_exif[tag_name] = decoded_value
        return decoded_exif

    def get_labels(self):
        # Extract all images file inside the folders and stored them into list
        sub_folder_path = os.path.join(main_path, directory)
        print(sub_folder_path)

        for image_file in os.listdir(sub_folder_path):
            if image_file.endswith(".jpg") or image_file.endswith(
                    ".png"):  # Check if the file ends with either '.jped' or '.png'
                image_path = os.path.join(sub_folder_path, image_file)
                # Read the image using OpenCV
                image = cv2.imread(image_path)  # the decoded images stored in **B G R** order.
                # Resize the image to a standard size
                image = cv2.resize(image, self.image_size)

                try:
                    img = Image.open(image_path)
                    exif_data = img._getexif()

                    if exif_data:
                        human_readable_exif = self.get_exif(exif_data)
                        #print(f"Human-readable EXIF data for {image_file}:")
                        for tag, value in human_readable_exif.items():
                            if tag == "UserComment":
                                temp = json.loads(value)
                                #print(f'WaterLevel: {temp["WaterLevel"]}')
                                # Append the image to the data list
                                self.data.append(image)
                                # Append the label to the labels list
                                self.labels.append(temp["WaterLevel"])
                    else:
                        print(f"No EXIF data found for {image_file}")
                except Exception as e:
                    print(f"Error processing {image_file} : {e}")

                # labels.append(sub_folder_path)

        # Convert the data and labels lists into numpy arrays
        self.data = np.array(self.data)
        self.labels = np.array(self.labels)

        # Print the dimension of dataset
        #print(f'data shape:{self.data.shape}')
        #print(f'labels shape:{self.labels.shape}')

        return self.data, self.labels

    def info(self):
        return self.input_sizeX, self.input_sizeY
