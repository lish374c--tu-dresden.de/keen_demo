from Data_acquisition_2 import AugmentedData
from sklearn.model_selection import train_test_split


class TrainTest:
    def __init__(self, test_size=0.2, random_state=42):
        self.X_train = []
        self.X_test = []
        self.Y_train = []
        self.Y_test = []
        self.imagedata = AugmentedData().image_generator()
        self.data = self.imagedata[0]
        self.labels = self.imagedata[1]
        self.test_size = test_size
        self.random_state = random_state

    def splitting(self):
        #print(self.data[0])
        #print(len(self.labels))
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.data,
                                                                                self.labels,
                                                                                test_size=self.test_size,
                                                                                random_state=self.random_state)

        return self.X_train, self.X_test, self.Y_train, self.Y_test

    def info(self):
        return self.data, self.labels
