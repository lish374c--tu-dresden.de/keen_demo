import numpy as np
import os
from Label_acquisition_1 import ImageLabels
from keras.preprocessing.image import ImageDataGenerator

os.path.dirname(os.getcwd())
main_path = os.getcwd()
os.chdir(main_path)

class AugmentedData:
    def __init__(self, n_gen=4, batch_size=32):
        self.imagedata = ImageLabels().get_labels()
        self.X = self.imagedata[0]
        self.Y = self.imagedata[1]
        self.n_gen = n_gen
        self.X_augmented = []
        self.Y_augmented = []
        self.batch_size = batch_size
        self.data = []
        self.labels = []


    def image_generator(self):

        datagen = ImageDataGenerator(
            rotation_range=0, #0
            width_shift_range=0.0,
            height_shift_range=0.0,
            shear_range=0.0,
            zoom_range=0.0,
            horizontal_flip=True,
            fill_mode='nearest')

        datagen.fit(self.X)

        for X_batch, y_batch in datagen.flow(self.X, self.Y, self.batch_size):
            self.X_augmented.append(X_batch)
            self.Y_augmented.append(y_batch)
            if len(self.X_augmented) >= 100:  # Setting generated augmented data
                break

        self.data = np.concatenate((self.X, np.concatenate(self.X_augmented)))
        self.labels = np.concatenate((self.Y, np.concatenate(self.Y_augmented)))

        #print(self.labels)
        return self.data, self.labels
